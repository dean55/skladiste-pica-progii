#define _CRT_SECURE_NO_WARNINGS
#include "deklaracije_funkcija_artikala.h"
#include "strukture.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

FILE* datoteka2;

void kreiranje_datoteke_2(void) {
	int broj = 0;
	if (datoteka2 = fopen("Bezalkoholna pica.bin", "rb")) {
		fclose(datoteka2);
	}
	else {
		datoteka2 = fopen("Bezalkoholna pica.bin", "wb");
		if (datoteka2 == NULL) {
			perror("kreiranje datoteke");
			exit(EXIT_FAILURE);
		}
		else {
			fwrite(&broj, sizeof(int), 1, datoteka2);
			fwrite(&broj, sizeof(int), 1, datoteka2);
			fclose(datoteka2);
		}
	}
}

int trazeni_id(void) {
	int trazeni_id;
	printf("Unesite id artikla\n");
	scanf(" %d", &trazeni_id);
	return trazeni_id;
}

int vrati_broj_artikala(void) {
	int broj;
	rewind(datoteka2);
	fread(&broj, sizeof(int), 1, datoteka2);
	return broj;
}

int vrati_id_2(void) {
	int broj;
	rewind(datoteka2);
	fseek(datoteka2, sizeof(int) * 1, SEEK_CUR);
	fread(&broj, sizeof(int), 1, datoteka2);
	return broj;
}
void zauzimanje_char(char** pok, int duljina) {
	*pok = (char*)calloc(duljina, sizeof(char));
	if (*pok == NULL) {
		perror("zauzimanje char");
		return;
	}
	fread(*pok, sizeof(char), duljina, datoteka2);
}

void zauzimanje_float(float** pok) {
	*pok = (float*)malloc(sizeof(float) * 1);
	if (*pok == NULL) {
		perror("zauzimanje float");
		return;
	}
	fread(*pok, sizeof(float), 1, datoteka2);
}

PICE* kreiraj_jpp_iz_datoteke(void) {
	PICE* glavni_cvor = (PICE*)calloc(1, sizeof(PICE));
	if (glavni_cvor == NULL) {
		perror("Kreiranje");
		return NULL;
	}
	else {
		unsigned short duljina = 0;
		rewind(datoteka2);
		fseek(datoteka2, sizeof(int) * 2, SEEK_CUR);
		fread(&glavni_cvor->id, sizeof(int), 1, datoteka2);
		fread(&duljina, sizeof(unsigned short), 1, datoteka2);
		zauzimanje_char(&glavni_cvor->naziv, duljina);
		fread(&glavni_cvor->gazirano, sizeof(char), 1, datoteka2);
		fread(&glavni_cvor->tip, sizeof(char), 1, datoteka2);
		fread(&glavni_cvor->kolicina, sizeof(int), 1, datoteka2);
		zauzimanje_float(&glavni_cvor->zapremina);
		zauzimanje_float(&glavni_cvor->cijena);
		glavni_cvor->sljedeci_cvor = NULL;

	}
	return glavni_cvor;
}

PICE* ubaci_novi_cvor_jpp_iz_datoteke(PICE* glavni_cvor) {
	PICE* novi_glavni_cvor = (PICE*)calloc(1, sizeof(PICE));
	if (novi_glavni_cvor == NULL) {
		perror("Kreiranje novog cvora");
		return glavni_cvor;
	}
	else {
		unsigned short duljina = 0;
		fread(&(novi_glavni_cvor)->id, sizeof(int), 1, datoteka2);
		fread(&duljina, sizeof(unsigned short), 1, datoteka2);
		zauzimanje_char(&novi_glavni_cvor->naziv, duljina);
		fread(&novi_glavni_cvor->gazirano, sizeof(char), 1, datoteka2);
		fread(&novi_glavni_cvor->tip, sizeof(char), 1, datoteka2);
		fread(&(novi_glavni_cvor)->kolicina, sizeof(int), 1, datoteka2);
		zauzimanje_float(&novi_glavni_cvor->zapremina);
		zauzimanje_float(&novi_glavni_cvor->cijena);
		novi_glavni_cvor->sljedeci_cvor = glavni_cvor;
	}
	return novi_glavni_cvor;
}

PICE* ucitavanje_pica(PICE* glavni_cvor, int broj_artikala) {
	if (broj_artikala == 1) {
		glavni_cvor = kreiraj_jpp_iz_datoteke();
		return glavni_cvor;
	}
	if (broj_artikala > 1) {
		int i;
		glavni_cvor = kreiraj_jpp_iz_datoteke();
		for (i = 1; i < broj_artikala; i++) {
			glavni_cvor = ubaci_novi_cvor_jpp_iz_datoteke(glavni_cvor);
		}
		return glavni_cvor;
	}
	return NULL;
}

void ispis_artikala(PICE* ispis_cvorova, int index) {
	int counter = 0;
	int zastavica = 0;
	if (ispis_cvorova == NULL) {
		perror("Ispis cvorova");
		exit(EXIT_FAILURE);
	}
	else {
		while (ispis_cvorova) {
			zastavica = 0;
			if (index == 0) {
				zastavica = 1;
			}
			else if (index == 1) {
				if (ispis_cvorova->tip == 'd') {
					zastavica = 1;
				}
			}
			else if (index == 2) {
				if (ispis_cvorova->tip == 'n') {
					zastavica = 1;
				}
			}
			if (zastavica == 1) {
				counter++;
				printf("%2d.   %3d   %s%s %s %s\t %d\t   %.2f L\t%.2f kn\n",
					counter,
					ispis_cvorova->id,
					ispis_cvorova->naziv,
					strlen(ispis_cvorova->naziv) < 4 ? "\t\t\t\t" : strlen(ispis_cvorova->naziv) < 12 ? "\t\t\t" : strlen(ispis_cvorova->naziv) < 18 ? "\t\t" : "\t",
					ispis_cvorova->gazirano == 'd' ? "gazirano\t" : "negazirano\t",
					ispis_cvorova->tip == 'd' ? "da\t" : "ne\t",
					ispis_cvorova->kolicina,
					*ispis_cvorova->zapremina,
					*ispis_cvorova->cijena);
			}
			ispis_cvorova = ispis_cvorova->sljedeci_cvor;
		}
	}
}

PICE* brisanje_jednostruko_povezanog_popisa(PICE* glavni_cvor) {
	PICE* izbrisi_cvor = NULL;
	while (glavni_cvor) {
		izbrisi_cvor = glavni_cvor;
		free(izbrisi_cvor->naziv);
		free(izbrisi_cvor->zapremina);
		free(izbrisi_cvor->cijena);
		glavni_cvor = glavni_cvor->sljedeci_cvor;
		free(izbrisi_cvor);
		/*printf("Oslobodjen chvor: %p\n", izbrisi_cvor);*/
	}
	return NULL;
}

PICE* dodaj_artikl_u_jpp(PICE* glavni_cvor, char* naziv_2, char gazirano, char tip, int kolicina, float zapremina_2, float cijena_2, int* id, int* broj_artikala) {
	PICE* novi_glavni_cvor = (PICE*)calloc(1, sizeof(PICE));
	if (novi_glavni_cvor == NULL) {
		perror("Kreiranje");
		return glavni_cvor;
	}
	else {
		(novi_glavni_cvor)->id = *id;
		(*id)++;
		(*broj_artikala)++;
		char* naziv = NULL;
		float* zapremina = NULL;
		float* cijena = NULL;
		naziv = (char*)malloc(sizeof(char) * (strlen(naziv_2) + 1));
		if (naziv == NULL) {
			perror("Dodavanje artikla: naziv");
			exit(EXIT_FAILURE);
		}
		strcpy(naziv, naziv_2);
		zapremina = (float*)malloc(sizeof(float));
		if (zapremina == NULL) {
			perror("Dodavanje artikla: zapremina");
			exit(EXIT_FAILURE);
		}
		*zapremina = zapremina_2;
		cijena = (float*)malloc(sizeof(float));
		if (cijena == NULL) {
			perror("Dodavanje artikla: cijena");
			exit(EXIT_FAILURE);
		}
		*cijena = cijena_2;
		novi_glavni_cvor->naziv = naziv;
		novi_glavni_cvor->gazirano = gazirano;
		novi_glavni_cvor->tip = tip;
		novi_glavni_cvor->kolicina = kolicina;
		novi_glavni_cvor->zapremina = zapremina;
		novi_glavni_cvor->cijena = cijena;
		novi_glavni_cvor->sljedeci_cvor = glavni_cvor;
	}
	return novi_glavni_cvor;
}

PICE* kreiranje_jpp(char* naziv_2, char gazirano, char tip, int kolicina, float zapremina_2, float cijena_2, int* id, int* broj_artikala) {
	PICE* glavni_cvor = (PICE*)malloc(1 * sizeof(PICE));
	if (glavni_cvor == NULL) {
		perror("Kreiranje");
		return NULL;
	}
	else {
		(glavni_cvor)->id = *id;
		(*id)++;
		(*broj_artikala)++;
		char* naziv = NULL;
		float* zapremina = NULL;
		float* cijena = NULL;
		naziv = (char*)malloc(sizeof(char) * (strlen(naziv_2) + 1));
		if (naziv == NULL) {
			perror("Dodavanje artikla: naziv");
			exit(EXIT_FAILURE);
		}
		strcpy(naziv, naziv_2);
		zapremina = (float*)malloc(sizeof(float));
		if (zapremina == NULL) {
			perror("Dodavanje artikla: zapremina");
			exit(EXIT_FAILURE);
		}
		*zapremina = zapremina_2;
		cijena = (float*)malloc(sizeof(float));
		if (cijena == NULL) {
			perror("Dodavanje artikla: cijena");
			exit(EXIT_FAILURE);
		}
		*cijena = cijena_2;
		glavni_cvor->naziv = naziv;
		glavni_cvor->gazirano = gazirano;
		glavni_cvor->tip = tip;
		glavni_cvor->kolicina = kolicina;
		glavni_cvor->zapremina = zapremina;
		glavni_cvor->cijena = cijena;
		glavni_cvor->sljedeci_cvor = NULL;
	}
	return glavni_cvor;
}

PICE* azuriranje_artikla(PICE* azuriranje, char* naziv_2, char gazirano, char tip, int kolicina, float zapremina_2, float cijena_2) {
	if (azuriranje == NULL) {
		perror("Kreiranje");
		return NULL;
	}
	else {
		free(azuriranje->naziv);
		char* naziv = NULL;
		naziv = (char*)malloc(sizeof(char) * (strlen(naziv_2) + 1));
		if (naziv == NULL) {
			perror("Dodavanje artikla: naziv");
			exit(EXIT_FAILURE);
		}
		strcpy(naziv, naziv_2);
		azuriranje->naziv = naziv;
		azuriranje->gazirano = gazirano;
		azuriranje->tip = tip;
		azuriranje->kolicina = kolicina;
		*azuriranje->zapremina = zapremina_2;
		*azuriranje->cijena = cijena_2;

	}
	return azuriranje;
}

void obrisi_artikl_iz_jpp(PICE** glavni_cvor, PICE* trazeni_cvor, int* broj_artikala) {
	(*broj_artikala)--;
	if (*glavni_cvor == trazeni_cvor) {
		*glavni_cvor = (*glavni_cvor)->sljedeci_cvor;
		free(trazeni_cvor->naziv);
		free(trazeni_cvor->zapremina);
		free(trazeni_cvor->cijena);
		free(trazeni_cvor);
	}
	else {
		PICE* pomocni_cvor = *glavni_cvor;
		while (pomocni_cvor->sljedeci_cvor) {
			if (pomocni_cvor->sljedeci_cvor == trazeni_cvor) {
				pomocni_cvor->sljedeci_cvor = trazeni_cvor->sljedeci_cvor;
				free(trazeni_cvor->naziv);
				free(trazeni_cvor->zapremina);
				free(trazeni_cvor->cijena);
				free(trazeni_cvor);
				break;
			}
			pomocni_cvor = pomocni_cvor->sljedeci_cvor;
		}
	}
}


char* naziv_artikla(char* naziv) {
	system("cls");
	if (naziv != NULL) {
		printf("Naziv vec postoji, ako ga zelite promjeniti pritisnite [d]\n");
		if (_getch() != 'd') {
			return naziv;
		}
	}
	char privremeno_polje[30];
	int i = 0, duljina = 0, zastavica = 0;
	do {
		if (naziv != NULL) {
			free(naziv);
			naziv = NULL;
		}
		system("cls");
		printf("Naziv artikla:");
		i = duljina = 0;
		do {
			fgets(privremeno_polje, 30, stdin);
		} while (*privremeno_polje == '\n');
		while (*(privremeno_polje + i) != '\n') {
			i++;
		}
		*(privremeno_polje + i) = '\0';
		duljina = strlen(privremeno_polje);
		if (duljina < 3) {
			printf("Naziv mora sadrzavati minimalno 2 slova\nza ponovni unos pritisnite [d]");
			if (_getch() != 'd') {
				return NULL;
			}
			else {
				zastavica = 1;
			}
		}
		else {
			zastavica = 0;
		}
	} while (zastavica);
	naziv = (char*)malloc(sizeof(char) * (duljina + 1));
	if (naziv == NULL) {
		perror("Zauzimanje memorije naziv");
		exit(EXIT_FAILURE);
	}
	strcpy(naziv, privremeno_polje);
	return naziv;
}

char gazirano_artikla(char gazirano) {
	system("cls");
	if (gazirano != 'x') {
		printf("\nVec je odabrano, ako zelite promjeniti pritisnite [d]\n");
		if (_getch() != 'd') {
			return gazirano;
		}
	}
	int zastavica = 0;
	char gazirano_1 = 'n';
	char gazirano_2 = 'd';
	char izbor;
	do {
		system("cls");
		printf("Tip artikla:");
		zastavica = 0;
		printf(" odaberite jednu od opcija\n1.negazirano\n2.gazirano\n");
		izbor = _getch();
		if (izbor == '1') {
			gazirano = gazirano_1;
			zastavica = 0;
		}
		else if (izbor == '2') {
			gazirano = gazirano_2;
			zastavica = 0;
		}
		else {
			zastavica = 1;
		}
	} while (zastavica);

	return gazirano;
}

char tip_artikla(char tip) {
	system("cls");
	if (tip != 'x') {
		printf("\nVec je odabrano, ako ga zelite promjeniti pritisnite [d]\n");
		if (_getch() != 'd') {
			return tip;
		}
	}
	int zastavica = 0;
	char tip_1 = 'd';
	char tip_2 = 'n';
	char izbor;
	do {
		system("cls");
		printf("Tip artikla:");
		zastavica = 0;
		printf(" odaberite jednu od opcija\n1.sadrzi alkohol\n2.bezalkoholno\n");
		izbor = _getch();
		if (izbor == '1') {
			tip = tip_1;
			zastavica = 0;
		}
		else if (izbor == '2') {
			tip = tip_2;
			zastavica = 0;
		}
		else {
			zastavica = 1;
		}
	} while (zastavica);

	return tip;
}

float zapremina_artikla(float zapremina) {
	system("cls");
	if (zapremina != 0.0) {
		printf("\nVec je uneseno, ako zelite promjeniti pritisnite [d]\n");
		if (_getch() != 'd') {
			return zapremina;
		}
	}
	printf("Unesite iznos u litrama\nZapremina:");
	scanf("%f", &zapremina);
	if (zapremina < 0) {
		zapremina *= -1;
	}
	return zapremina;
}

float cijena_artikla(float cijena) {
	system("cls");
	if (cijena != 0.0) {
		printf("\nVec je uneseno, ako zelite promjeniti pritisnite [d]\n");
		if (_getch() != 'd') {
			return cijena;
		}
	}
	if (cijena < 0) {
		cijena *= -1;
	}
	printf("Unesite iznos u kunam\nCijena:");
	scanf("%f", &cijena);
	return cijena;
}

int kolicina_artikla(void) {
	int kolicina = 0;
	system("cls");
	printf("Kolicina:");
	scanf(" %d", &kolicina);
	return kolicina;
}

int id_artikla(void) {
	int id, id_2;
	rewind(datoteka2);
	fseek(datoteka2, sizeof(int) * 1, SEEK_CUR);
	fread(&id, sizeof(int), 1, datoteka2);
	id_2 = id;
	id_2++;
	rewind(datoteka2);
	fseek(datoteka2, sizeof(int) * 1, SEEK_CUR);
	fwrite(&id_2, sizeof(int), 1, datoteka2);
	return id;
}

int izlaz_artikl(char* naziv) {
	system("cls");
	printf("Jeste li sigurni da zelite izaci, za potvrdu pritisnite [d]\n");
	if (_getch() == 'd') {
		if (naziv != NULL) {
			free(naziv);
		}
		return 0;
	}
	return 1;
}

PICE* id_pretraga_jpp(PICE* trazeni_cvor, int id) {
	while (trazeni_cvor) {
		if (trazeni_cvor->id == id) {
			return trazeni_cvor;
		}
		else {
			trazeni_cvor = trazeni_cvor->sljedeci_cvor;
		}
	}
	return NULL;
}

void ispis_trenutnog_cvora(PICE* trazeni_cvor) {
	printf("id:%3d naziv:%s%s %s tip:%s kolicina:%d zapremina:%.2fL cijena:%.2fkn\n",
		trazeni_cvor->id,
		trazeni_cvor->naziv,
		strlen(trazeni_cvor->naziv) < 6 ? "\t\t\t" : strlen(trazeni_cvor->naziv) < 14 ? "\t\t" : "\t",
		trazeni_cvor->gazirano == 'd' ? "gazirano\t" : "negazirano\t",
		trazeni_cvor->tip == 'd' ? "sadrzi alkohol  " : "bezalkoholno  ",
		trazeni_cvor->kolicina,
		*trazeni_cvor->zapremina,
		*trazeni_cvor->cijena);
}

void id_pretraga_ispis(PICE* glavni_cvor, PICE* trazeni_cvor) {
	trazeni_cvor = id_pretraga_jpp(glavni_cvor, trazeni_id());
	system("cls");
	if (trazeni_cvor == NULL) {
		printf("Trazeni artikl nije pronadjen, pritisnite bilo koju tipku za povratak");
		_getch();
	}
	else {
		ispis_trenutnog_cvora(trazeni_cvor);
		printf("Pritisnite bilo koju tipku za povratak");
		_getch();
	}
}

PICE* naziv_pretraga_jpp(PICE* trazeni_cvor, char* naziv) {
	while (trazeni_cvor) {
		if (strcmp(trazeni_cvor->naziv, naziv) == 0) {
			return trazeni_cvor;
		}
		else {
			trazeni_cvor = trazeni_cvor->sljedeci_cvor;
		}
	}
	return NULL;
}

void naziv_pretraga_ispis(PICE* glavni_cvor, PICE* trazeni_cvor) {
	printf("Unesite naziv artikla kojeg zelite pronaci\n");
	int i = 0;
	char* naziv = NULL;
	char privremeno_polje[30] = { 0 };
	do {
		fgets(privremeno_polje, 30, stdin);
	} while (*privremeno_polje == '\n');
	while (*(privremeno_polje + i) != '\n') {
		i++;
	};
	*(privremeno_polje + i) = '\0';
	naziv = (char*)malloc(sizeof(char) * (strlen(privremeno_polje) + 1));
	strcpy(naziv, privremeno_polje);
	if (naziv == NULL) {
		perror("Zauzimanje memorije, pretraga naziv");
		exit(EXIT_FAILURE);
	}
	trazeni_cvor = naziv_pretraga_jpp(glavni_cvor, naziv);
	if (naziv != NULL) {
		free(naziv);
		naziv = NULL;
	}
	system("cls");
	if (trazeni_cvor == NULL) {
		printf("Trazeni artikl nije pronadjen, pritisnite bilo koju tipku za povratak");
		_getch();
	}
	else {
		ispis_trenutnog_cvora(trazeni_cvor);
		printf("Pritisnite bilo koju tipku za povratak");
		_getch();
	}
}

PICE* bouble_sort_jpp(PICE* glavni_cvor, int broj_artikala, int index)
{
	if (broj_artikala == 0) {
		return glavni_cvor;
	}
	PICE* temp = glavni_cvor;
	int zamjena = 0;
	int temp_id = 0;
	char* temp_naziv;
	char temp_gazirano;
	char temp_tip;
	int temp_kolicina = 0;
	float* temp_zapremina;
	float* temp_cijena;
	for (int i = 0; i < broj_artikala; i++)
	{
		temp = glavni_cvor;
		while (temp->sljedeci_cvor != NULL)
		{
			zamjena = 0;
			if (index == 1) {
				if (strcmp(temp->naziv, temp->sljedeci_cvor->naziv) > 0) {
					zamjena = 1;
				}
			}
			else if (index == 2) {
				if (strcmp(temp->naziv, temp->sljedeci_cvor->naziv) < 0) {
					zamjena = 1;
				}
			}
			else if (index == 3) {
				if (*temp->cijena < *temp->sljedeci_cvor->cijena) {
					zamjena = 1;
				}
			}
			else if (index == 4) {
				if (*temp->cijena > *temp->sljedeci_cvor->cijena) {
					zamjena = 1;
				}
			}
			if (zamjena == 1) {
				temp_id = temp->id;
				temp->id = temp->sljedeci_cvor->id;
				temp->sljedeci_cvor->id = temp_id;
				temp_naziv = temp->naziv;
				temp->naziv = temp->sljedeci_cvor->naziv;
				temp->sljedeci_cvor->naziv = temp_naziv;
				temp_gazirano = temp->gazirano;
				temp->gazirano = temp->sljedeci_cvor->gazirano;
				temp->sljedeci_cvor->gazirano = temp_gazirano;
				temp_tip = temp->tip;
				temp->tip = temp->sljedeci_cvor->tip;
				temp->sljedeci_cvor->tip = temp_tip;
				temp_kolicina = temp->kolicina;
				temp->kolicina = temp->sljedeci_cvor->kolicina;
				temp->sljedeci_cvor->kolicina = temp_kolicina;
				temp_zapremina = temp->zapremina;
				temp->zapremina = temp->sljedeci_cvor->zapremina;
				temp->sljedeci_cvor->zapremina = temp_zapremina;
				temp_cijena = temp->cijena;
				temp->cijena = temp->sljedeci_cvor->cijena;
				temp->sljedeci_cvor->cijena = temp_cijena;
			}
			temp = temp->sljedeci_cvor;
		}
	}
	return glavni_cvor;
}

void zapisivanje_jpp_u_datoteku(PICE* glavni_cvor, int id, int broj_artikala) {
	datoteka2 = fopen("Bezalkoholna pica.bin", "wb");
	if (datoteka2 == NULL) {
		perror("Zapisivanje jpp-a u datoteku");
		exit(EXIT_FAILURE);
	}
	else {
		rewind(datoteka2);
		fwrite(&broj_artikala, sizeof(int), 1, datoteka2);
		fwrite(&id, sizeof(int), 1, datoteka2);
		while (glavni_cvor) {
			unsigned short duljina = 0;
			duljina = strlen(glavni_cvor->naziv);
			duljina++;
			fwrite(&glavni_cvor->id, sizeof(int), 1, datoteka2);
			fwrite(&duljina, sizeof(unsigned short), 1, datoteka2);
			fwrite(glavni_cvor->naziv, sizeof(char), duljina, datoteka2);
			fwrite(&glavni_cvor->gazirano, sizeof(char), 1, datoteka2);
			fwrite(&glavni_cvor->tip, sizeof(char), 1, datoteka2);
			fwrite(&glavni_cvor->kolicina, sizeof(int), 1, datoteka2);
			fwrite(glavni_cvor->zapremina, sizeof(float), 1, datoteka2);
			fwrite(glavni_cvor->cijena, sizeof(float), 1, datoteka2);
			glavni_cvor = glavni_cvor->sljedeci_cvor;
		}
		fclose(datoteka2);
	}
}