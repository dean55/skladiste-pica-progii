#ifndef HEADER_KOTISNIK
#define HEADER_KOTISNIK

#include "strukture.h"

int login_forma(void);
int pocetni_izbornik(void);
int admin_mod_izbornik(int index);
int korisnici(void);
int datotek_postoji_ili_ne(void);
int kreiranje_datoteke(void);
int broj_korisnika(void);
int forma(void);
void dodaj_korisnika(char* ime, char* prezime, char* opis_posla, char* korisnicko_ime, char* lozinka);
void ispisivanje_korisnika(const KORISNIK* const korisnici, int broj_korisnika, int index);
KORISNIK* ucitaj_korisnike(KORISNIK* racuni, int broj_racuna);
void brisanje_korisnika(const KORISNIK* const korisnici, int broj_korisnika, int id, int id_2);
int pretrazivanje_korisnika_putem_ida(KORISNIK* const korisnici, int broj_korisnika);
int provjera_znakova(char* lozinka);
char* izbaci_prelazak_u_novi_red(char* string);
char* unos_2(char* string);
char* unos_lozinke_prijava_2(char* lozinka2);
char* unos_lozinke_prijava(char* lozinka);
char* unos_lozinke_2(char* lozinka2);
char* unos_lozinke_1(char* lozinka);
char* sakriji_lozinku(char* lozinka);
int usporedba_lozinki(char* lozinka, char* lozinka_2);
char generiraj_slovo(void);
char* prosiri_lozinku(char* lozinka);
char* hash_lozinke(char* lozinka, int duzina_lozinke);
int provjera_duzine_lozinke(char* lozinka);
void oslobadjanje_forme(char*, char*, char*, char*, char*, char*, char*, char*);
int izlaz(void);
int provjera_korisnik_istog_imena(char* korisnicko_ime);
void sortiranje(KORISNIK* racuni, int broj_racuna, int vrsta_sortiranja);
int azuriranje(int trazeni_id);
KORISNIK* pronadji_korisnika(KORISNIK* racuni, int trazeni_id, int broj_racuna);
void oslobadjanje_forme_azuriranja(char* ime, char* prezime, char* opis_posla, char* korisnicko_ime);
void azuriranje_datoteke(KORISNIK* racuni, char* ime, char* prezime, char* opis_posla, char* korisnicko_ime, char* status, int broj_racuna, int trazeni_id);
char* dodjela_prava(char* status);
char* zauzimanje_memorije(char* string);
int provjera_korisnik_lozinka(char* korisnicko_ime, char* lozinka);
int provjera_statusa(char* status);
int provjera_lozinke(char* hash_lozinka, char* hash_lozinka_2);
int broj_znakova_lozinke(char* hash_lozinka);
void oslobađanje_forme_login(char* korisnicko_ime, char* lozinka, char* hash_lozinka);
int vrati_id(KORISNIK* const korisnici, char* korisnicko_ime, int broj_racuna);
char* vrati_korisnicko_ime(KORISNIK* const racuni, int id, int broj_racuna);
int vrati_broj_zahtjeva(KORISNIK* const racuni, int broj_racuna);
int provjera_admin(char* korisnicko_ime, char* lozinka);
void ispis_tekstualne_datoteke(void);
char* ucitaj_znak_rekurzija(int i);
char generiraj_slovo_veliko(void);
char ubaci_slovo(void);
char* generiraj_kod_za_potvrdu(void);
void brisanje_datoteke_korisnika(int index);

#endif