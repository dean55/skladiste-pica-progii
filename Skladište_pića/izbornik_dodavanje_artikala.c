#define _CRT_SECURE_NO_WARNINGS
#include "deklaracije_funkcija_artikala.h"
#include "strukture.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static char* naziv = NULL;
static char gazirano = 'x', tip = 'x';
static int kolicina = 0;
static int status_naziv = 0, status_gazirano = 0, status_tip = 0, status_kolicina = 0, status_zapremina = 0, status_cijena = 0;
static float zapremina = 0.0, cijena = 0.0;
int id;
int broj_artikala;
static int brojac = 0;
PICE* glavni_cvor;
PICE* azuriranje_pica;

int izbornik_dodaj_artikl(int index) {
	char izbor;
	if (index == 1 && brojac == 0) {
		brojac++;
		naziv = (char*)malloc(sizeof(char) * (strlen(azuriranje_pica->naziv) + 1));
		if (naziv == NULL) {
			perror("Zauzimanje memorije azuriranje naziv");
		}
		strcpy(naziv, azuriranje_pica->naziv);
		gazirano = azuriranje_pica->gazirano;
		tip = azuriranje_pica->tip;
		kolicina = azuriranje_pica->kolicina;
		zapremina = *azuriranje_pica->zapremina;
		cijena = *azuriranje_pica->cijena;
		status_naziv = status_gazirano = status_tip = status_kolicina = status_zapremina = status_cijena = 1;
	}
	system("cls");
	printf("########################################################################################################################\n");
	printf("\n");
	printf(" 1.Naziv artikla:%s\n", naziv == NULL ? "" : naziv);
	printf("\n");
	printf(" 2.Gazirano     :%s\n", gazirano == 'x' ? "" : gazirano == 'd' ? "gazirano" : "negazirano");
	printf("\n");
	printf(" 3.Tip          :%s\n", tip == 'x' ? "" : tip == 'd' ? "sadrzi alkohol" : "bezalkoholno");
	printf("\n");
	printf(" 4.Kolicina     :%d\n", kolicina);
	printf("\n");
	printf(" 5.Zapremina    :%.2f Litara\n", zapremina);
	printf("\n");
	printf(" 6.Cijena       :%.2f kn\n", cijena);
	printf("\n");
	printf(" 7.Dodaj(azuriraj) artikl\n");
	printf("\n");
	printf(" 8.Izlaz\n");
	printf("\n");
	printf("########################################################################################################################\n");
	izbor = _getch();
	switch (izbor)
	{
	case'1':
		naziv = naziv_artikla(naziv);
		if (naziv == NULL) {
			status_naziv = 0;
		}
		status_naziv = 1;
		return 1;
		break;
	case'2':
		gazirano = gazirano_artikla(gazirano);
		if (gazirano == 'x') {
			status_gazirano = 0;
			return 1;
		}
		status_gazirano = 1;
		return 1;
		break;
	case'3':
		tip = tip_artikla(tip);
		if (tip == 'x') {
			status_tip = 0;
			return 1;
		}
		status_tip = 1;
		return 1;
		break;
	case '4':
		kolicina = kolicina_artikla();
		if (kolicina != 0) {
			status_kolicina = 1;
		}
		return 1;
		break;
	case '5':
		zapremina = zapremina_artikla(zapremina);
		if (zapremina != 0.0) {
			status_zapremina = 1;
		}
		return 1;
		break;
	case '6':
		cijena = cijena_artikla(cijena);
		if (cijena != 0.0) {
			status_cijena = 1;
		}
		return 1;
		break;
	case '7':
		system("cls");
		if (status_naziv == 1 && status_gazirano == 1 && status_tip == 1 && status_kolicina == 1 && status_zapremina == 1 && status_cijena == 1) {
			if (index == 0) {
				if (broj_artikala == 0) {
					glavni_cvor = kreiranje_jpp(naziv, gazirano, tip, kolicina, zapremina, cijena, &id, &broj_artikala);
				}
				else {
					glavni_cvor = dodaj_artikl_u_jpp(glavni_cvor, naziv, gazirano, tip, kolicina, zapremina, cijena, &id, &broj_artikala);
				}
			}
			else if (index == 1) {
				azuriranje_pica = azuriranje_artikla(azuriranje_pica, naziv, gazirano, tip, kolicina, zapremina, cijena);
			}
			free(naziv);
			naziv = NULL;
			status_naziv = 0, status_gazirano = 0, status_tip = 0, status_kolicina = 0, status_zapremina = 0, status_cijena = 0;
			zapremina = cijena = 0.0;
			kolicina = brojac = 0;
			gazirano = tip = 'x';
			printf("Artikl je uspjesno dodan(azuriran), pritisnite bilo koju tipku");
			_getch();
			return 0;
		}
		printf("Sva polja moraju biti ispunjena i kolicina treba biti razlicita od 0\npritisnite bilo koju tipku");
		_getch();
		return 1;
		break;
	case '8':
		if (izlaz_artikl(naziv) == 0) {
			naziv = NULL;
			status_naziv = 0, status_gazirano = 0, status_tip = 0, status_kolicina = 0, status_zapremina = 0, status_cijena = 0;
			kolicina = brojac = 0;
			zapremina = cijena = 0.0;
			gazirano = tip = 'x';
			return 0;
		}
		return 1;
	case '9':
		printf("naziv:%s\t\tstatus:%d\ngazirano:%c\t\tstatus:%d\ntip:%c\t\tstatus:%d\nkolicina:%d\t\tstatus:%d\nzapremina:%.2f\t\tstatus:%d\ncijena:%.2f\t\tstatus:%d",
			naziv, status_naziv,
			gazirano, status_gazirano,
			tip, status_tip,
			kolicina, status_kolicina,
			zapremina, status_zapremina,
			cijena, status_cijena);
		_getch();
		return 1;
		break;
	default:
		return 1;
		break;
	}
}