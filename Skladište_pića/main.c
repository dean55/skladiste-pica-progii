#define _CRT_SECURE_NO_WARNINGS
#include "deklaracije_funkcija.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

extern FILE* datoteka;

int main(void) {
	srand((unsigned)time(NULL));
	if (datotek_postoji_ili_ne() == 0) {
		return 0;
	}
	datoteka = fopen("korisnicki racuni.bin", "rb+");
	if (datoteka == NULL) {
		perror("Otvaranje datoteke");
		return 0;
	}
	while (pocetni_izbornik());
	fclose(datoteka);
	return 0;
}