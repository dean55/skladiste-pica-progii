#ifndef HEADER_2
#define HEADER_2

#include "strukture.h"


int izbornik_dodaj_artikl(int index);
int izbornik_artikli(int index);
void kreiranje_datoteke_2(void);
int vrati_broj_artikala(void);
int trazeni_id(void);
int vrati_id_2(void);
void zauzimanje_char(char** pok, int duljina);
void zauzimanje_float(float** pok);
PICE* kreiraj_jpp_iz_datoteke(void);
PICE* ubaci_novi_cvor_jpp_iz_datoteke(PICE* glavni_cvor);
PICE* ucitavanje_pica(PICE* glavni_cvor, int broj_artikala);
void zapisivanje_jpp_u_datoteku(PICE* glavni_cvor, int id, int broj_artikala);
void ispis_artikala(PICE* ispis_cvorova, int index);
PICE* brisanje_jednostruko_povezanog_popisa(PICE* glavni_cvor);
PICE* id_pretraga_jpp(PICE* trazeni_cvor, int id);
void ispis_trenutnog_cvora(PICE* trazeni_cvor);
void id_pretraga_ispis(PICE* glavni_cvor, PICE* trazeni_cvor);
PICE* naziv_pretraga_jpp(PICE* trazeni_cvor, char* naziv);
void naziv_pretraga_ispis(PICE* glavni_cvor, PICE* trazeni_cvor);
//void dodaj_artikl(void);
//void dodaj_artikl(char* naziv, char* tip, int kolicina);
PICE* kreiranje_jpp(char* naziv_2, char gazirano, char tip, int kolicina, float zapremina_2, float cijena_2, int* id, int* broj_artikala);
PICE* dodaj_artikl_u_jpp(PICE* glavni_cvor, char* naziv_2, char gazirano, char tip, int kolicina, float zapremina_2, float cijena_2, int* id, int* broj_artikala);
void obrisi_artikl_iz_jpp(PICE** glavni_cvor, PICE* trazeni_cvor, int* broj_artikala);
PICE* azuriranje_artikla(PICE* azuriranje, char* naziv_2, char gazirano, char tip, int kolicina, float zapremina_2, float cijena_2);
PICE* bouble_sort_jpp(PICE* glavni_cvor, int broj_artikala, int index);
//PICE* bouble_sort_z_a_jpp(PICE* glavni_cvor, int broj_artikala);
char* naziv_artikla(char* naziv);
char gazirano_artikla(char gazirano);
char tip_artikla(char tip);
int kolicina_artikla(void);
float zapremina_artikla(float zapremina);
float cijena_artikla(float cijena);
int id_artikla(void);
int izlaz_artikl(char* naziv);

#endif // !HEADER_2