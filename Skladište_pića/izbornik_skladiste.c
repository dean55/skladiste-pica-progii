#define _CRT_SECURE_NO_WARNINGS
#include "deklaracije_funkcija_artikala.h"
#include "strukture.h"
#include <stdio.h>
#include <stdlib.h>
#include <windows.system.h>
#include <conio.h>

PICE* glavni_cvor;
extern PICE* azuriranje_pica = NULL;
static PICE* trazeni_cvor = NULL;
int broj_artikala;
int id;
static int ispis = 0;


int izbornik_artikli(int index) {

	char izbor;
	system("cls");
	printf("#######################################################################################################################\n");
	printf("R.br.  id   naziv                       sadrzi CO2   sadrzi alkohol   kolicina   zapremina      cijena\n");
	printf("#######################################################################################################################\n");
	if (broj_artikala == 0) {
		printf("Nema artikala za prikaz\n");
	}
	if (broj_artikala != 0) {
		ispis_artikala(glavni_cvor, ispis);
	}
	printf("#######################################################################################################################\n");
	printf("Ukupni broj artikala:%d\n", broj_artikala);
	if (index == 0) {
		printf("  (1) Dodaj artikl   (2) Obrisi artikl   (3) Azuriraj artikl ");
	}
	printf("  (4) Pretraga putem id - a   (5) Pretraga naziva\n");
	printf("  Sortiranje naziva : (6) a-z  (7) z-a  cijene : (8) max-min (9) min-max\n");
	printf("  Ispis  (s) svih pica [def]  (a) koja sadrze alkohol  (b) bezalkoholna pica\n");
	printf("  (x) izlaz\n");
	printf("#######################################################################################################################\n");
	izbor = _getch();
	switch (izbor)
	{
	case '1':
		if (index == 1) {
			return 1;
		}
		while (izbornik_dodaj_artikl(0));
		return 1;
		break;
	case '2':
		if (index == 1) {
			return 1;
		}
		if (broj_artikala == 0) {
			printf("Nema artikala, pritisni bilo koju tipku");
			_getch();
			return 1;
		}
		trazeni_cvor = id_pretraga_jpp(glavni_cvor, trazeni_id());
		if (trazeni_cvor == NULL) {
			printf("Ne postoji artikl s unesenim id-om");
			_getch();
			return 1;
		}
		obrisi_artikl_iz_jpp(&glavni_cvor, trazeni_cvor, &broj_artikala);
		trazeni_cvor = NULL;
		return 1;
		break;
	case '3':
		if (index == 1) {
			return 1;
		}
		azuriranje_pica = id_pretraga_jpp(glavni_cvor, trazeni_id());
		if (azuriranje_pica == NULL) {
			return 1;
		}
		while (izbornik_dodaj_artikl(1));
		return 1;
		break;
	case '4':
		id_pretraga_ispis(glavni_cvor, trazeni_cvor);
		return 1;
		break;
	case '5':
		naziv_pretraga_ispis(glavni_cvor, trazeni_cvor);
		return 1;
		break;

	case '6':
		glavni_cvor = bouble_sort_jpp(glavni_cvor, broj_artikala, 1);
		return 1;
		break;
	case '7':
		glavni_cvor = bouble_sort_jpp(glavni_cvor, broj_artikala, 2);
		return 1;
		break;
	case '8':
		glavni_cvor = bouble_sort_jpp(glavni_cvor, broj_artikala, 3);
		return 1;
		break;
	case '9':
		glavni_cvor = bouble_sort_jpp(glavni_cvor, broj_artikala, 4);
		return 1;
		break;
	case 'a':
		ispis = 1;
		return 1;
		break;
	case 'b':
		ispis = 2;
		return 1;
		break;
	case 's':
		ispis = 0;
		return 1;
		break;
	case 'x':
		if (glavni_cvor != NULL) {
			zapisivanje_jpp_u_datoteku(glavni_cvor, id, broj_artikala);
		}
		if (glavni_cvor != NULL) {
			glavni_cvor = brisanje_jednostruko_povezanog_popisa(glavni_cvor);
		}
		if (trazeni_cvor != NULL) {
			trazeni_cvor = NULL;
		}
		ispis = 0;
		return 0;
		break;
	default:
		return 1;
		break;
	}
}