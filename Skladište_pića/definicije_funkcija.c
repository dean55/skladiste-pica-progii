#define _CRT_SECURE_NO_WARNINGS
#include "deklaracije_funkcija.h"
#include "deklaracije_funkcija_artikala.h"
#include "strukture.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

FILE* datoteka_3 = NULL;
FILE* datoteka = NULL;
KORISNIK admin;

int kreiranje_datoteke(void) {
	int broj = 0, ID = 0;
	datoteka = fopen("korisnicki racuni.bin", "wb");
	if (datoteka == NULL) {
		perror("Kreiranje datoteke korisnicki racuni.bin");
		return -1;
	}
	else {
		fwrite(&broj, sizeof(int), 1, datoteka);
		fwrite(&ID, sizeof(int), 1, datoteka);
		fclose(datoteka);
		return 1;
	}
}

int datotek_postoji_ili_ne(void) {
	if (datoteka = fopen("korisnicki racuni.bin", "rb")) {
		fclose(datoteka);
		return 1;
	}
	else if (kreiranje_datoteke() == -1) {
		return 0;
	}
	else {
		return 1;
	}
}

int broj_korisnika(void) {
	int broj;
	rewind(datoteka);
	fread(&broj, sizeof(int), 1, datoteka);
	return broj;
}

KORISNIK* ucitaj_korisnike(KORISNIK* racuni, int broj_racuna) {
	racuni = (KORISNIK*)calloc(broj_racuna, sizeof(KORISNIK));
	if (racuni == NULL) {
		perror("Kreiranje");
		return NULL;
	}
	else {
		rewind(datoteka);
		fseek(datoteka, sizeof(int) * 2, SEEK_CUR);
		fread(racuni, sizeof(KORISNIK), broj_racuna, datoteka);
	}
	return racuni;
}

void dodaj_korisnika(char* ime, char* prezime, char* opis_posla, char* korisnicko_ime, char* lozinka) {
	int broj_korisnika, id;
	rewind(datoteka);
	fread(&broj_korisnika, sizeof(int), 1, datoteka);
	fread(&id, sizeof(int), 1, datoteka);
	fseek(datoteka, sizeof(KORISNIK) * broj_korisnika, SEEK_CUR);
	KORISNIK temp = { 0 };
	temp.id = id;
	strcpy(temp.ime, ime);
	strcpy(temp.prezime, prezime);
	strcpy(temp.opis_posla, opis_posla);
	strcpy(temp.korisnicko_ime, korisnicko_ime);
	strcpy(temp.hash_lozinka, lozinka);
	strcpy(temp.status, "NDEF");
	fwrite(&temp, sizeof(KORISNIK), 1, datoteka);
	rewind(datoteka);
	broj_korisnika++;
	fwrite(&broj_korisnika, sizeof(int), 1, datoteka);
	id++;
	fwrite(&id, sizeof(int), 1, datoteka);
}

int pretrazivanje_korisnika_putem_ida(KORISNIK* const korisnici, int broj_korisnika) {
	if (korisnici == NULL) {
		printf("Nema korisnika, pritisnite bilo koju tipku\n");
		_getch();
		return -1;
	}
	int i;
	int trazeni_id = 0;
	printf("Unesite id korisnika\n");
	scanf("%d", &trazeni_id);
	for (i = 0; i < broj_korisnika; i++)
	{
		if (trazeni_id == (korisnici + i)->id) {
			printf("Korisnik je pronaden!\n");
			return trazeni_id;
		}
	}
	printf("Nema korisnika sa tim id-om, pritisni bilo koju tipku\n");
	_getch();
	return -2;
}

void brisanje_korisnika(const KORISNIK* const korisnici, int broj_korisnika, int id, int id_2) {
	fclose(datoteka);
	datoteka = fopen("korisnicki racuni.bin", "wb");
	if (datoteka == NULL) {
		perror("Brisanje korisnika iz datoteke studenti.bin");
		exit(EXIT_FAILURE);
	}
	rewind(datoteka);
	fseek(datoteka, sizeof(int) * 2, SEEK_CUR);
	int i;

	int brojac_korisnika = 0;
	for (i = 0; i < broj_korisnika; i++)
	{
		if (id != (korisnici + i)->id) {
			fwrite((korisnici + i), sizeof(KORISNIK), 1, datoteka);
			brojac_korisnika++;
		}
	}
	rewind(datoteka);
	fwrite(&brojac_korisnika, sizeof(int), 1, datoteka);
	fwrite(&id_2, sizeof(int), 1, datoteka);
	fclose(datoteka);
	datoteka = fopen("korisnicki racuni.bin", "rb+");
	if (datoteka == NULL) {
		perror("Brisanje korisnika iz datoteke studenti.bin");
		exit(EXIT_FAILURE);
	}
}

void ispisivanje_korisnika(const KORISNIK* const korisnici, int broj_korisnika,int index) {
	int i,ispisi=0;
	for (i = 0; i < broj_korisnika; i++)
	{
		ispisi = 0;
		if (index == 0) {
			ispisi = 1;
		}
		if (strcmp((korisnici + i)->status, "NDEF") == 0) {
			ispisi = 1;
		}
		if (ispisi == 1) {
			printf("    %d.   %3d    %s%s%s%s%s%s%s%s%s\n",
				i + 1,
				(korisnici + i)->id,
				(korisnici + i)->ime,
				strlen((korisnici + i)->ime) < 8 ? "\t\t\t" : strlen((korisnici + i)->ime) < 16 ? "\t\t" : "\t",
				(korisnici + i)->prezime,
				strlen((korisnici + i)->prezime) < 8 ? "\t\t\t" : strlen((korisnici + i)->prezime) < 16 ? "\t\t" : "\t",
				(korisnici + i)->opis_posla,
				strlen((korisnici + i)->opis_posla) < 8 ? "\t\t\t" : strlen((korisnici + i)->opis_posla) < 16 ? "\t\t" : "\t",
				(korisnici + i)->korisnicko_ime,
				strlen((korisnici + i)->korisnicko_ime) < 8 ? "\t\t" : "\t",
				(korisnici + i)->status);
		}
	}
}

int provjera_duzine_lozinke(char* lozinka) {
	int broj_znakova = strlen(lozinka);

	if (broj_znakova < 8) {
		printf("\nNeispravna lozinka : potrebno je unjeti min. 8 znakova\n");
		return 25;
	}
	else if (broj_znakova > 15) {
		printf("\nNeispravna lozinka : dozvoljeno max. 15 znakova\n");
		return 25;
	}
	return 0;
}

int provjera_znakova(char* lozinka) {
	int i = 0;
	int nedozvoljeni_znak = 0;
	int broj_znakova = strlen(lozinka);
	for (i = 0; i < broj_znakova; i++) {
		if (*(lozinka + i) < 46) {
			nedozvoljeni_znak++;
		}
		if (*(lozinka + i) == 47) {
			nedozvoljeni_znak++;
		}
		if (*(lozinka + i) > 57 && *(lozinka + i) < 97) {
			nedozvoljeni_znak++;
		}
		if (*(lozinka + i) > 122) {
			nedozvoljeni_znak++;
		}

	}
	return nedozvoljeni_znak;
}

char* izbaci_prelazak_u_novi_red(char* string) {
	int i = 0;
	while (*(string + i) != '\n') {
		i++;
	}
	*(string + i) = '\0';
	return string;
}

char* unos_2(char* string) {
	if (string == NULL) {
		char pomocno_polje[31] = { 0 };
		int duljina = 0;
		do {
			duljina = 0;
			do {
				fgets(pomocno_polje, 30, stdin);
			} while (*pomocno_polje == '\n');
			duljina = strlen(pomocno_polje);
			if (duljina <= 2) {
				printf("mora sadrzavati minimalno 2 znaka\nPonovi unos:");
			}
		} while (duljina <= 2);
		string = (char*)calloc(duljina + 1, sizeof(char));
		if (string == NULL) {
			return NULL;
		}
		strcpy(string, pomocno_polje);
		string = izbaci_prelazak_u_novi_red(string);
		return string;
	}
	else if (string != NULL) {
		printf(" vec postoji, za ponovni unos pritisnite [d]");
		if (_getch() == 'd') {
			printf("\nnovi unos:");
			free(string);
			string = NULL;
			char pomocno_polje[31] = { 0 };
			int duljina = 0;
			do {
				duljina = 0;
				fgets(pomocno_polje, 30, stdin);
				duljina = strlen(pomocno_polje);
				if (duljina <= 2) {
					printf("mora sadrzavati minimalno 2 znaka\nime:");
				}
			} while (duljina <= 2);
			string = (char*)calloc(duljina + 1, sizeof(char));
			if (string == NULL) {
				return NULL;
			}
			strcpy(string, pomocno_polje);
			string = izbaci_prelazak_u_novi_red(string);
			return string;
		}
		else {
			return string;
		}
	}
	printf("\nPogreska u unos_2 funkciji, izvan okvira");
	return NULL;
}

char* unos_lozinke_prijava_2(char*lozinka2) {
	int zastavica = 0, duljina = 0, i = 0;
	char privremeno_polje[17] = { 0 };
	do {
		zastavica = duljina = i = 0;
		if (lozinka2 != NULL) {
			free(lozinka2);
			lozinka2 = NULL;
		}
		printf("Lozinka:");
		do {
			*(privremeno_polje + i) = _getch();
			if (*(privremeno_polje + i) != '\r') {
				printf("*");
			}
			i++;
		} while (*(privremeno_polje + i - 1) != '\r' && i != 17);
		*(privremeno_polje + i - 1) = '\0';
		duljina = strlen(privremeno_polje);
		if (duljina > 15) {
			printf("Lozinka sadrzi previse znakova\nZelite li pokusati ponovo da[d] ili ne[press any key]\n");
			if (_getch() == 'd') {
				zastavica = 1;
			}
			else {
				free(lozinka2);
				return NULL;
			}
		}
		else {
			lozinka2 = (char*)malloc((duljina + 1) * sizeof(char));
			if (lozinka2 == NULL) {
				perror("Lozinka, zauzimanje memorije");
				return NULL;
			}
			strcpy(lozinka2, privremeno_polje);
			zastavica = 0;
		}
	} while (zastavica);
	return lozinka2;
}

char* unos_lozinke_prijava(char*lozinka) {
	if (lozinka == NULL) {
		system("cls");
		lozinka = unos_lozinke_prijava_2(lozinka);
		if (lozinka == NULL) {
			return NULL;
		}
		return lozinka;
	}
	else if (lozinka != NULL) {
		system("cls");
		printf("Lozinka vec postoji, ako zelite unjeti novu pritisnite [d]\n");
		if (_getch() != 'd') {
			return lozinka;
		}
		free(lozinka);
		lozinka = NULL;
		lozinka = unos_lozinke_prijava_2(lozinka);
		if (lozinka == NULL) {
			return NULL;
		}
		return lozinka;
	}
	printf("\nNiti jedan od uvijeta");
	return lozinka;
}

char* unos_lozinke_2(char* lozinka2) {
	int zastavica = 0, duljina = 0, i = 0;
	char privremeno_polje[17] = { 0 };
	do {
		zastavica = duljina = i = 0;
		if (lozinka2 != NULL) {
			free(lozinka2);
			lozinka2 = NULL;
		}
		printf("Lozinka:");
		do {
			*(privremeno_polje + i) = _getch();
			if (*(privremeno_polje + i) != '\r') {
				printf("*");
			}
			i++;
		} while (*(privremeno_polje + i - 1) != '\r' && i != 17);
		*(privremeno_polje + i - 1) = '\0';
		duljina = strlen(privremeno_polje);
		lozinka2 = (char*)malloc((duljina + 1) * sizeof(char));
		if (lozinka2 == NULL) {
			perror("Lozinka, zauzimanje memorije");
			return NULL;
		}
		strcpy(lozinka2, privremeno_polje);
		if (provjera_duzine_lozinke(lozinka2) == 25) {
			printf("Ako zelite pokusati ponovo pritisnite [d]\n");
			if (_getch() == 'd') {
				zastavica = 1;
			}
			else {
				free(lozinka2);
				return NULL;
			}
		}
		if (zastavica == 0) {
			if (provjera_znakova(lozinka2) != 0) {
				printf("\nUnjeli ste nedozvoljeni znak\nZelite li pokusati ponovo pritisnite[d]\n");
				if (_getch() == 'd') {
					zastavica = 1;
				}
				else {
					free(lozinka2);
					return NULL;
				}
			}
		}
	} while (zastavica);
	return lozinka2;
}

char* unos_lozinke_1(char* lozinka) {
	if (lozinka == NULL) {
		system("cls");
		printf("Lozinka treba sadrzavati 8-15 znakova, dozvoljeni znakovi su [.] [0,9] [a,z]\n");
		lozinka = unos_lozinke_2(lozinka);
		if (lozinka == NULL) {
			return NULL;
		}
		return lozinka;
	}
	else if (lozinka != NULL) {
		system("cls");
		printf("Lozinka vec postoji, ako zelite unjeti novu pritisnite [d]\n");
		if (_getch() != 'd') {
			return lozinka;
		}
		free(lozinka);
		lozinka = NULL;
		lozinka = unos_lozinke_2(lozinka);
		if (lozinka == NULL) {
			return NULL;
		}
		return lozinka;
	}
	printf("\nNiti jedan od uvijeta");
	return lozinka;
}

char* sakriji_lozinku(char* lozinka) {
	char* pok = NULL;
	char pomocno_polje[17] = { 0 };
	int duljina = 0, i = 0;
	if (lozinka == NULL) {
		return NULL;
	}
	duljina = strlen(lozinka);
	for (i = 0; i < duljina; i++) {
		*(pomocno_polje + i) = '*';
	}
	*(pomocno_polje + i) = '\0';
	pok = (char*)malloc((duljina + 1) * sizeof(char));
	if (pok == NULL) {
		perror("Sakrij lozinku : zauzimanje memorije");
		return NULL;
	}
	strcpy(pok, pomocno_polje);
	return pok;
}

int usporedba_lozinki(char* lozinka, char* lozinka_2) {
	if (strcmp(lozinka, lozinka_2) == 0) {
		return 1;
	}
	printf("\nLozinke se ne poklapaju, ponovo unesite lozinku\nPritisnite bilo koju tipku za nastavak");
	_getch();
	return 0;
}

char generiraj_slovo(void) {
	char slovo;
	slovo = 97 + (float)rand() / RAND_MAX * (122 - 97);
	return slovo;
}

char generiraj_slovo_veliko(void) {
	char slovo;
	slovo = 65 + (float)rand() / RAND_MAX * (90 - 65);
	return slovo;
}

char* prosiri_lozinku(char* lozinka) {
	/*printf("\nLozinka\t\t\t:%s", lozinka);*/
	int i = 0;
	int duzina_lozinke;
	char* prosirena_lozinka = NULL;
	prosirena_lozinka = (char*)malloc(16 * sizeof(char));
	if (prosirena_lozinka == NULL) {
		perror("prosirena lozinka, zauzimanje memorije");
		return NULL;
	}

	duzina_lozinke = strlen(lozinka);

	for (i = 0; i < duzina_lozinke; i++) {
		*(prosirena_lozinka + i) = *(lozinka + i);
	}
	for (i = duzina_lozinke; i < 15; i++) {
		*(prosirena_lozinka + i) = generiraj_slovo();
	}
	*(prosirena_lozinka + 15) = '\0';
	/*printf("\nProsirena lozinka\t:%s", prosirena_lozinka);*/
	return prosirena_lozinka;
}

char* hash_lozinke(char* lozinka, int duzina_lozinke) {
	char* hash_lozinka = NULL;
	int i, j = 0;
	hash_lozinka = (char*)malloc(33 * sizeof(char));
	if (hash_lozinka == NULL) {
		perror("hash lozinka,zauzimanje memorije");
		return NULL;
	}
	for (i = 0; i < 15; i++) {
		*(hash_lozinka + j) = *(lozinka + i) + 4;
		*(hash_lozinka + j + 1) = *(lozinka + i) + 2;
		j += 2;
	}
	switch (duzina_lozinke)
	{
	case 8:
		*(hash_lozinka + 30) = 'a';
		*(hash_lozinka + 31) = 's';
		break;
	case 9:
		*(hash_lozinka + 30) = 'd';
		*(hash_lozinka + 31) = 'f';
		break;
	case 10:
		*(hash_lozinka + 30) = 'g';
		*(hash_lozinka + 31) = 'h';
		break;
	case 11:
		*(hash_lozinka + 30) = 'j';
		*(hash_lozinka + 31) = 'k';
		break;
	case 12:
		*(hash_lozinka + 30) = 'y';
		*(hash_lozinka + 31) = 'x';
		break;
	case 13:
		*(hash_lozinka + 30) = 'c';
		*(hash_lozinka + 31) = 'v';
		break;
	case 14:
		*(hash_lozinka + 30) = 'b';
		*(hash_lozinka + 31) = 'n';
		break;
	case 15:
		*(hash_lozinka + 30) = 'm';
		*(hash_lozinka + 31) = 'j';
		break;
	default:
		printf("\nLozinka je izvan okvira predvidjene duzine");
		break;
	}
	*(hash_lozinka + 32) = '\0';
	/*printf("\nHash lozinka\t\t:%s", hash_lozinka);*/
	return hash_lozinka;
}

int izlaz(void) {
	system("cls");
	printf("Jeste li sigurni da zelite izaci\nPritisnite [d] za potvrdu");
	if (_getch() == 'd') {
		return 1;
	}
	return 0;
}

void oslobadjanje_forme(char* ime, char* prezime, char* opis_posla, char* korisnicko_ime, char* lozinka, char* lozinka_2, char* hash_lozinka, char* hash_lozinka_2) {
	if (ime != NULL) {
		free(ime);
	}
	if (prezime != NULL) {
		free(prezime);
	}
	if (opis_posla != NULL) {
		free(opis_posla);
	}
	if (korisnicko_ime != NULL) {
		free(korisnicko_ime);
	}
	if (lozinka != NULL) {
		free(lozinka);
	}
	if (lozinka_2 != NULL) {
		free(lozinka_2);
	}
	if (hash_lozinka != NULL) {
		free(hash_lozinka);
	}
	if (hash_lozinka_2 == NULL) {
		free(hash_lozinka_2);
	}
}

void oslobadjanje_forme_azuriranja(char* ime, char* prezime, char* opis_posla, char* korisnicko_ime) {
	if (ime != NULL) {
		free(ime);
	}
	if (prezime != NULL) {
		free(prezime);
	}
	if (opis_posla != NULL) {
		free(opis_posla);
	}
	if (korisnicko_ime != NULL) {
		free(korisnicko_ime);
	}
}

int provjera_korisnik_istog_imena(char* korisnicko_ime) {
	int broj_racuna, i;
	KORISNIK* racuni = NULL;
	broj_racuna = broj_korisnika();
	racuni = ucitaj_korisnike(racuni, broj_racuna);
	if (racuni == NULL) {
		perror("Ucitavanje racuna");
		return 0;
	}
	else {
		for (i = 0; i < broj_racuna; i++) {
			if (strcmp((racuni + i)->korisnicko_ime, korisnicko_ime) == 0) {
				free(racuni);
				racuni = NULL;
				return 1;
			}
		}
		free(racuni);
		racuni = NULL;
		return 2;
	}
}

void sortiranje(KORISNIK* racuni, int broj_racuna, int vrsta_sortiranja) {
	KORISNIK temp = { 0 };
	int i, j;
	rewind(datoteka);
	fseek(datoteka, sizeof(int) * 2, SEEK_CUR);
	if (vrsta_sortiranja == 1) {
		for (i = 0; i < broj_racuna; i++) {
			for (j = (i + 1); j < broj_racuna; j++) {
				if (strcmp((racuni + i)->ime, (racuni + j)->ime) > 0) {
					temp = *(racuni + i);
					*(racuni + i) = *(racuni + j);
					*(racuni + j) = temp;
				}
			}
		}
	}
	if (vrsta_sortiranja == 2) {
		for (i = 0; i < broj_racuna; i++) {
			for (j = (i + 1); j < broj_racuna; j++) {
				if (strcmp((racuni + i)->ime, (racuni + j)->ime) < 0) {
					temp = *(racuni + i);
					*(racuni + i) = *(racuni + j);
					*(racuni + j) = temp;
				}
			}
		}
	}
	if (vrsta_sortiranja == 3) {
		for (i = 0; i < broj_racuna; i++) {
			for (j = (i + 1); j < broj_racuna; j++) {
				if (strcmp((racuni + i)->prezime, (racuni + j)->prezime) > 0) {
					temp = *(racuni + i);
					*(racuni + i) = *(racuni + j);
					*(racuni + j) = temp;
				}
			}
		}
	}
	if (vrsta_sortiranja == 4) {
		for (i = 0; i < broj_racuna; i++) {
			for (j = (i + 1); j < broj_racuna; j++) {
				if (strcmp((racuni + i)->prezime, (racuni + j)->prezime) < 0) {
					temp = *(racuni + i);
					*(racuni + i) = *(racuni + j);
					*(racuni + j) = temp;
				}
			}
		}
	}
	fwrite(racuni, sizeof(KORISNIK), broj_racuna, datoteka);
}

KORISNIK* pronadji_korisnika(KORISNIK* racuni, int trazeni_id, int broj_racuna) {
	KORISNIK* temp = { 0 };
	int i;
	for (i = 0; i < broj_racuna; i++) {
		if ((racuni + i)->id == trazeni_id) {
			return (racuni + i);
		}
	}
	return NULL;
}

char* dodjela_prava(char* status) {
	int zastavica = 0;
	if (status != NULL) {
		printf("status vec postoji, ako ga zelite promjeniti pritisnite [d]");
		if (_getch() != 'd') {
			return status;
		}
	}
	do {
		zastavica = 0;
		printf("Za dodjelu prava odaberite jednu od opcija\n1.ADMIN\n2.MODERATOR\n3.NDEF\n");
		char izbor;
		char* string_1 = { "ADMIN" };
		char* string_2 = { "MODERATOR" };
		char* string_3 = { "NDEF" };
		izbor = _getch();
		if (izbor == '1') {
			return string_1;
		}
		if (izbor == '2') {
			return string_2;
		}
		if (izbor == '3') {
			return string_3;
		}
		else {
			printf("niste odabrali niti jednu od ponudjenih opcija\nzelite li pokusati ponovo pritisnite [d]");
			if (_getch() == 'd') {
				zastavica = 1;
			}
			else {
				zastavica = 0;
			}
		}
	} while (zastavica);
	return status;
}

char* zauzimanje_memorije(char* string) {
	char* pok = NULL;
	int duzina_stringa = 0;
	duzina_stringa = strlen(string);
	pok = (char*)malloc(sizeof(char) * (duzina_stringa + 1));
	if (pok == NULL) {
		return 0;
	}
	strcpy(pok, string);
	return pok;
}

void azuriranje_datoteke(KORISNIK* racuni, char* ime, char* prezime, char* opis_posla, char* korisnicko_ime, char* status, int broj_racuna, int trazeni_id) {
	int i;
	for (i = 0; i < broj_racuna; i++) {
		if (trazeni_id == (racuni + i)->id) {
			strcpy((racuni + i)->ime, ime);
			strcpy((racuni + i)->prezime, prezime);
			strcpy((racuni + i)->opis_posla, opis_posla);
			strcpy((racuni + i)->korisnicko_ime, korisnicko_ime);
			strcpy((racuni + i)->status, status);
		}
	}
	rewind(datoteka);
	fseek(datoteka, sizeof(int) * 2, SEEK_CUR);
	fwrite(racuni, sizeof(KORISNIK), broj_racuna, datoteka);
}

int broj_znakova_lozinke(char* hash_lozinka) {
	if (*(hash_lozinka + 30) == 'a' && *(hash_lozinka + 31) == 's') {
		return 8;
	}
	else if (*(hash_lozinka + 30) == 'd' && *(hash_lozinka + 31) == 'f') {
		return 9;
	}
	else if (*(hash_lozinka + 30) == 'g' && *(hash_lozinka + 31) == 'h') {
		return 10;
	}
	else if (*(hash_lozinka + 30) == 'j' && *(hash_lozinka + 31) == 'k') {
		return 11;
	}
	else if (*(hash_lozinka + 30) == 'y' && *(hash_lozinka + 31) == 'x') {
		return 12;
	}
	else if (*(hash_lozinka + 30) == 'c' && *(hash_lozinka + 31) == 'v') {
		return 13;
	}
	else if (*(hash_lozinka + 30) == 'b' && *(hash_lozinka + 31) == 'n') {
		return 14;
	}
	else if (*(hash_lozinka + 30) == 'm' && *(hash_lozinka + 31) == 'j') {
		return 15;
	}
	return -1;
}

int provjera_lozinke(char* hash_lozinka,char* hash_lozinka_2) {
	/*printf("\nHash lozinke 1 :%s\nHash lozinke 2 :%s", hash_lozinka,hash_lozinka_2);*/
	int broj_znakova_1=0,broj_znakova_2=0, i;
	broj_znakova_1 = broj_znakova_lozinke(hash_lozinka);
	broj_znakova_2 = broj_znakova_lozinke(hash_lozinka_2);
	/*printf("\nbroj1:%d  broj2:%d\n", broj_znakova_1, broj_znakova_2);*/
	if (broj_znakova_1 == -1) {
		return 0;
	}
	if (broj_znakova_2 == -1) {
		return 0;
	}
	broj_znakova_1 *= 2;
	char temp_hash_1[33] = { 0 };
	for (i = 0; i < broj_znakova_1; i++) {
		temp_hash_1[i] = *(hash_lozinka + i);
	}
	temp_hash_1[i + 1] = '\0';
	broj_znakova_2 *= 2;
	char temp_hash_2[33] = { 0 };
	for (i = 0; i < broj_znakova_2; i++) {
		temp_hash_2[i] = *(hash_lozinka_2 + i);
	}
	temp_hash_2[i + 1] = '\0';
	/*printf("\nHash lozinke 1 :%s\nHash lozinke 2 :%s", temp_hash_1, temp_hash_2);*/
	/*if (strcmp(temp_hash_1, admin.hash_lozinka) == 0) {
		return 1;
	}*/
	if (strcmp(temp_hash_1, temp_hash_2) == 0) {
		return 1;
	}
	return 0;
}

int provjera_statusa(char* status) {
	if (strcmp(status,"ADMIN")==0) {
		return 2;
	}
	if (strcmp(status,"MODERATOR")==0) {
		return 3;
	}
	if (strcmp(status,"NDEF")==0){
		return 4;
	}
	return 0;
}

int provjera_korisnik_lozinka(char* korisnicko_ime, char* lozinka) {
	char* prosirena_lozinka = NULL,* hash_lozinka = NULL;
	int i,status=0;
	KORISNIK* racuni = NULL;
	int broj_racuna = broj_korisnika();
	int provjera_korisnickog_imena = 0;
	provjera_korisnickog_imena = provjera_korisnik_istog_imena(korisnicko_ime);
	if (provjera_korisnickog_imena == 0) {
		if (korisnicko_ime != NULL) {
			free(korisnicko_ime);
		}
		if (lozinka != NULL) {
			free(lozinka);
		}
		korisnicko_ime = lozinka = NULL;
		return 0;
	}
	else if (provjera_korisnickog_imena == 2) {
		return 0;
	}
	racuni = ucitaj_korisnike(racuni, broj_racuna);
	for (i = 0; i < broj_racuna; i++) {
		if (strcmp((racuni + i)->korisnicko_ime, korisnicko_ime) == 0) {
			
			prosirena_lozinka = prosiri_lozinku(lozinka);
			if (prosirena_lozinka == NULL) {
				perror("Prosirena lozinka");
				return 0;
			}
			hash_lozinka = hash_lozinke(prosirena_lozinka, strlen(lozinka));
			/*printf("\nHash lozinka %s", hash_lozinka);*/
			if (hash_lozinka == NULL) {
				return 0;
			}
			status=provjera_lozinke(hash_lozinka, (racuni + i)->hash_lozinka);
			/*printf("\nStatus:%d", status);*/
			if (status == 1) {
				status = provjera_statusa((racuni + i)->status);
				/*printf("\nStatus:%d", status);*/
				if (status == 2) {
					return 2;
				}
				if (status == 3) {
					return 3;
				}
				if (status == 4) {
					return 4;
				}
			}
			

		}
	}
	return 0;
}

void osloba�anje_forme_login(char* korisnicko_ime, char* lozinka, char* hash_lozinka) {
	if (korisnicko_ime != NULL) {
		free(korisnicko_ime);
	}
	if (lozinka != NULL) {
		free(lozinka);
	}
	if (hash_lozinka != NULL) {
		free(hash_lozinka);
	}
}

int vrati_id(KORISNIK* const korisnici,char* korisnicko_ime, int broj_racuna) {
	if (korisnici == NULL) {
		printf("Nema korisnika, pritisnite bilo koju tipku\n");
		_getch();
		return -1;
	}
	int i;
	for (i = 0; i < broj_racuna; i++)
	{
		if (strcmp((korisnici + i)->korisnicko_ime,korisnicko_ime)==0) {
			return (korisnici + i)->id;
		}
	}
	return -2;
}

char* vrati_korisnicko_ime(KORISNIK* const racuni, int id , int broj_racuna) {
	if (racuni == NULL) {
		printf("Nema korisnika, pritisnite bilo koju tipku\n");
		_getch();
		return NULL;
	}
	int i;
	for (i = 0; i < broj_racuna; i++)
	{
		if ((racuni + i)->id == id) {
			return (racuni + i)->korisnicko_ime;
		}
	}
	return NULL;
}

int vrati_broj_zahtjeva(KORISNIK* const racuni, int broj_racuna) {
	if (racuni == NULL) {
		printf("Nema korisnika, pritisnite bilo koju tipku\n");
		_getch();
		return 0;
	}
	int i;
	int broj_zahtjeva=0;
	for (i = 0; i < broj_racuna; i++)
	{
		if (strcmp((racuni + i)->status, "NDEF")==0) {
			broj_zahtjeva++;
		}
	}
	return broj_zahtjeva;
}

int provjera_admin(char* korisnicko_ime, char* lozinka) {
	int status = 0;
	char* prosirena_lozinka = NULL, * hash_lozinka = NULL;
	if (strcmp(korisnicko_ime, admin.korisnicko_ime) == 0) {
		prosirena_lozinka = prosiri_lozinku(lozinka);
		if (prosirena_lozinka == NULL) {
			perror("Prosirena lozinka");
			return 0;
		}
		hash_lozinka = hash_lozinke(prosirena_lozinka, strlen(lozinka));
		/*printf("\nHash lozinka %s", hash_lozinka);*/
		if (hash_lozinka == NULL) {
			return 0;
		}
		status = provjera_lozinke(hash_lozinka, admin.hash_lozinka);
		if (status == 1) {
			return -5;
		}
	}
	return 0;
}

char* ucitaj_znak_rekurzija(int i) {
	char znak;
	static char* polje = NULL;
	if ((znak = fgetc(datoteka_3)) != EOF) {
		/*printf("%d.%c ",i,znak);*/
		i++;
		polje = ucitaj_znak_rekurzija(i);
	}
	else {
		polje = (char*)malloc((i + 1) * sizeof(char));
		if (polje == NULL) {
			perror("Zauzimanje memorije tekstualne datoteke");
			exit(EXIT_FAILURE);
		}
		*(polje + i) = '\0';
	}
	*(polje + (i - 1)) = znak;
	/*printf("\n%d.%c", i, *(polje + (i - 1)));*/
	return polje;
}

void ispis_tekstualne_datoteke(void) {
	fclose(datoteka);
	char* polje = NULL;
	datoteka_3 = fopen("o programu.txt", "r");
	if (datoteka_3 == NULL) {
		perror("Otvaranje tekstualne datoteke");
		exit(EXIT_FAILURE);
	}
	else {
		polje = ucitaj_znak_rekurzija(0);
		system("cls");
		printf("%s", polje);
		free(polje);
		fclose(datoteka_3);
	}
	datoteka = fopen("korisnicki racuni.bin", "rb+");
	if (datoteka == NULL) {
		perror("Otvaranje datoteke");
		exit(EXIT_FAILURE);
	}
	do{
		printf("\nPritisni [x] za povratak");
	} while (_getch() != 'x');
}

char ubaci_slovo(void) {
	int izbor = 0;
	izbor = 1 + (float)rand() / RAND_MAX * (2 - 1 + 1);
	if (izbor == 1) {
		return generiraj_slovo();
	}
	return generiraj_slovo_veliko();
}

char* generiraj_kod_za_potvrdu(void) {
	int i;
	char* polje = NULL;
	polje = (char*)malloc(11 * sizeof(char));
	if (polje == NULL) {
		perror("Zauzimanje memorije generiranog koda za potvrdu");
		exit(EXIT_FAILURE);
	}
	else {
		for (i = 0; i < 10; i++) {
			*(polje + i) = ubaci_slovo();
		}
		*(polje + 10) = '\0';
	}
	return polje;
}

void brisanje_datoteke_korisnika(int index) {
	int status=-1,i=0;
	char* generirani_kod = NULL;
	char pomocno_polje[40] = { 0 };
	generirani_kod = generiraj_kod_za_potvrdu();
	printf("\nUnesite kod za potvrdu\n%s\n", generirani_kod);
	do {
		fgets(pomocno_polje, 40, stdin);
	} while (*pomocno_polje == '\n');
	while (*(pomocno_polje + i) != '\n') {
		i++;
	}
	*(pomocno_polje + i) = '\0';
	if (index == 0) {
		if (strcmp(generirani_kod, pomocno_polje) == 0) {
			status = remove("korisnicki racuni.bin");
			if (status == 0) {
				printf("Uspjesno obrisana datoteka, pritisnite bilo koju tipku\n");
				if (kreiranje_datoteke() == -1) {
					perror("kreiranje datoteke nakon brisanja");
					exit(EXIT_FAILURE);
				}
				_getch();
			}
		}
	}
	if (index == 1) {
		if (strcmp(generirani_kod, pomocno_polje) == 0) {
			status = remove("Bezalkoholna pica.bin");
			if (status == 0) {
				printf("Uspjesno obrisana datoteka, pritisnite bilo koju tipku\n");
				kreiranje_datoteke_2();
				_getch();
			}
		}
	}
	
	if(status!=0) {
		printf("Pogre�no unesen kod, pritisnite bilo koju tipku\n");
		_getch();
	}

}